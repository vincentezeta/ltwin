// Project1.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Project1.h"
#include <vector>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name
HWND hFrame = NULL, hEdit = NULL, hNote = NULL, hDraw = NULL, hChoice = NULL;
long heiMon = 0, widMon = 0;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
ATOM				MyRegisterClassNote(HINSTANCE hInstance);
ATOM				MyRegisterClassDraw(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
BOOL				InitInstanceNote(HINSTANCE, int);
BOOL				InitInstanceDraw(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	WndProcNote(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	WndProcDraw(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	Choice(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_PROJECT1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);
	MyRegisterClassNote(hInstance);
	MyRegisterClassDraw(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PROJECT1));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

ATOM MyRegisterClassNote(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProcNote;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= L"P1_NOTE";
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

ATOM MyRegisterClassDraw(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProcDraw;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= NULL;
	wcex.hCursor		= LoadCursor(NULL, IDC_CROSS);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= NULL;
	wcex.lpszClassName	= L"P1_DRAW";
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}


//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hFrame = CreateWindow(szWindowClass, NULL, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hFrame)
   {
      return FALSE;
   }

   ShowWindow(hFrame, SW_SHOWMAXIMIZED);
   UpdateWindow(hFrame);

   return TRUE;
}

BOOL InitInstanceNote(HINSTANCE hInstance, int nCmdShow)
{
   hNote = CreateWindow(L"P1_NOTE", NULL, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hNote)
   {
      return FALSE;
   }

   SetWindowPos(hNote, HWND_NOTOPMOST, 0.053*widMon, 0.1*heiMon,
	   0.421*widMon, 0.8*heiMon, SWP_SHOWWINDOW);

   RECT rec; GetClientRect(hNote, &rec);
   HRGN reg = CreateRoundRectRgn(rec.left, rec.top, rec.right,
	   rec.bottom, rec.right/19, rec.bottom/19);
   SetWindowRgn(hNote, reg, true);

   ShowWindow(hNote, SW_SHOW);
   UpdateWindow(hNote);

   return TRUE;
}

BOOL InitInstanceDraw(HINSTANCE hInstance, int nCmdShow)
{
   hDraw = CreateWindow(L"P1_DRAW", NULL, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hDraw)
   {
      return FALSE;
   }

   SetWindowPos(hDraw, HWND_NOTOPMOST, 0.526*widMon, 0.1*heiMon,
	   0.421*widMon, 0.8*heiMon, SWP_SHOWWINDOW);

   RECT rec; GetClientRect(hDraw, &rec);
   HRGN reg = CreateRoundRectRgn(rec.left, rec.top, rec.right,
	   rec.bottom, rec.right/19, rec.bottom/19);
   SetWindowRgn(hDraw, reg, true);

   ShowWindow(hDraw, SW_SHOW);
   UpdateWindow(hDraw);

   return TRUE;
}


//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static HINSTANCE hinstLib = NULL;
	void (*ProcAddr)(HWND);

	switch (message)
	{
	case WM_CREATE:
	{
		HMONITOR hMon = MonitorFromWindow(hWnd, MONITOR_DEFAULTTONEAREST);
		MONITORINFO iMon; iMon.cbSize = sizeof(MONITORINFO);
		GetMonitorInfo(hMon, &iMon);
		heiMon = iMon.rcMonitor.bottom - iMon.rcMonitor.top;
		widMon = iMon.rcMonitor.right - iMon.rcMonitor.left; //

		SetWindowLong(hWnd, GWL_STYLE, 0);
		SetWindowLong(hWnd, GWL_EXSTYLE, WS_EX_LAYERED/* | WS_EX_TOOLWINDOW*/);
		SetLayeredWindowAttributes(hWnd, 0, (255 * 50) / 100, LWA_ALPHA);

		InitInstanceNote(hInst, SW_SHOW);
		InitInstanceDraw(hInst, SW_SHOW);
		
		hChoice = CreateDialog(hInst, MAKEINTRESOURCE(IDD_CHOICE), hWnd, Choice);
		RECT rec; GetClientRect(hChoice, &rec);
		SetWindowPos(hChoice, HWND_NOTOPMOST, 0.947*widMon-rec.right, 0.1*heiMon-1.5*rec.bottom,
			rec.right, rec.bottom, SWP_SHOWWINDOW);
		ShowWindow(hChoice, SW_SHOW);

		hinstLib = LoadLibrary(L"Classifier.dll"); //
		if (hinstLib != NULL)
		{
			ProcAddr = (void (*)(HWND))GetProcAddress(hinstLib, "TheHook"); //
			if (ProcAddr != NULL)
				ProcAddr(hWnd);
		}
		break;
	}
	case WM_RBUTTONDOWN:
	{
		if (wParam == IDC_HOOK)
		{
			if (IsWindowVisible(hWnd))
			{
				ShowWindow(hWnd, SW_HIDE);
				ShowWindow(hNote, SW_HIDE);
				ShowWindow(hDraw, SW_HIDE);
				ShowWindow(hChoice, SW_HIDE);
			}
			else
			{
				ShowWindow(hWnd, SW_SHOW);
				ShowWindow(hNote, SW_SHOW);
				ShowWindow(hDraw, SW_SHOW);
				ShowWindow(hChoice, SW_SHOW);
			}
		}
		break;
	}
	case WM_UNICHAR:
	{
		if (wParam == 0x1EC4)
			SendMessage(hChoice, WM_COMMAND, IDC_F, 0);
		break;
	}
	case MM_CHAR:
	{
		TCHAR mes[2] = { 0 };
		mes[0] = (TCHAR)wParam;
		MessageBox(hWnd, mes, L"I", MB_OK);
		std::vector<std::vector<POINT>>* (*gp)() =
			(std::vector<std::vector<POINT>>* (*)())GetProcAddress(hinstLib, "GetPoint");
		std::vector<std::vector<POINT>>* vec = gp();
		HDC hdc = GetDC(hWnd);
		HPEN hPen = CreatePen(PS_SOLID, 20, RGB(0, 0, 255));
		SelectObject(hdc, hPen);
		SetROP2(hdc, R2_COPYPEN);
		for (unsigned int i = 0; i < vec->size(); ++i)
		for (unsigned int j = 0; j < (*vec)[i].size(); ++j)
		{
			MoveToEx(hdc, (*vec)[i][j].x, (*vec)[i][j].y, NULL);
			LineTo(hdc, (*vec)[i][j].x, (*vec)[i][j].y);
		}
		DeleteObject(hPen);
		ReleaseDC(hWnd, hdc);
		break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
	{
		void (*Unh)() = (void (*)())GetProcAddress(hinstLib, "UnTheHook");
		if (Unh != NULL)
			Unh();
		FreeLibrary(hinstLib);
		PostQuitMessage(0);
		break;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK WndProcNote(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_CREATE:
	{
		SetWindowLong(hWnd, GWL_STYLE, 0);
		SetWindowLong(hWnd, GWL_EXSTYLE, WS_EX_TOOLWINDOW);

		hEdit = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_VISIBLE | ES_MULTILINE | ES_AUTOVSCROLL, 
			0, 0, 0, 0, hWnd, NULL, hInst, 0L);
		ShowWindow(hEdit, SW_SHOW);
		break;
	}
	case WM_SIZE:
	{
		RECT rec; GetClientRect(hWnd, &rec);
		int incW = 0.011*widMon, incH = 0.014*heiMon;
		MoveWindow(hEdit, rec.left+incW, rec.top+incH, rec.right-rec.left-incW-incH, rec.bottom-rec.top-incW-incH, 0);
		break;
	}
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		SendMessage(hFrame, WM_DESTROY, 0, 0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

static double dist(double x, double y)
{
	return sqrt(x*x+y*y);
}

LRESULT CALLBACK WndProcDraw(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	static int x1, y1, x2, y2, type = IDC_F;
	static HPEN hPen;
	static bool finishedL = 1, finishedF = 1;
	static std::wostringstream woss;

	switch (message)
	{
	case WM_CREATE:
	{
		SetWindowLong(hWnd, GWL_STYLE, 0);
		SetWindowLong(hWnd, GWL_EXSTYLE, WS_EX_TOOLWINDOW);

		hPen = CreatePen(PS_SOLID, 10, RGB(0, 0, 0));
		break;
	}
	case WM_RBUTTONDOWN:
		if (HIWORD(GetKeyState(VK_MENU)))
			SendMessage(hWnd, WM_DESTROY, 0, 0);
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case IDC_R: case IDC_E: case IDC_L: case IDC_F:
			type = wmId;
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	
	case WM_LBUTTONDOWN:
	{
		if (type == IDC_L /*|| type == IDC_F*/)
		{
			if (!finishedL)
			{
				hdc = GetDC(hWnd);
				SelectObject(hdc, hPen);
			
				SetROP2(hdc, R2_NOTXORPEN);
				MoveToEx(hdc, x1, y1, NULL);
				LineTo(hdc, x2, y2);

				SetROP2(hdc, R2_COPYPEN);
				MoveToEx(hdc, x1, y1, NULL);
				LineTo(hdc, x2, y2);
		
				ReleaseDC(hWnd, hdc);
			}
		
			finishedL = 0;
		}
		else if (type == IDC_F)
		{
			//
			finishedF = 0;
			// 
		}

		x1 = x2 = LOWORD(lParam);
		y1 = y2 = HIWORD(lParam);
		break;
	}

	case WM_MOUSEMOVE:
	{
		if (!(wParam & MK_LBUTTON))
			break;

		hdc = GetDC(hWnd);
		SelectObject(hdc, hPen);
		SetROP2(hdc, R2_NOTXORPEN);

		switch (type)
		{
		case IDC_R:
			Rectangle(hdc, x1, y1, x2, y2);
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			Rectangle(hdc, x1, y1, x2, y2);
			break;
		case IDC_E:
			Ellipse(hdc, x1, y1, x2, y2);
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			Ellipse(hdc, x1, y1, x2, y2);
			break;
		case IDC_L:
			if (!(x1 == x2 && y1 == y2))
			{
				MoveToEx(hdc, x1, y1, NULL);
				LineTo(hdc, x2, y2);
			}
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			MoveToEx(hdc, x1, y1, NULL); 
			LineTo(hdc, x2, y2);
			break;
		case IDC_F:
		{
			SetROP2(hdc, R2_COPYPEN);
			if (finishedF)
			{
				MoveToEx(hdc, x1, y1, NULL);
				LineTo(hdc, x2, y2);
			}
			MoveToEx(hdc, x2, y2, NULL);
			LineTo(hdc, LOWORD(lParam), HIWORD(lParam));
			x2 = LOWORD(lParam);
			y2 = HIWORD(lParam);
			// 
			break;
		}
		}

		ReleaseDC(hWnd, hdc);
		break;
	}

	case WM_LBUTTONUP:
	{
		hdc = GetDC(hWnd);
		SelectObject(hdc, hPen);
		SetROP2(hdc, R2_NOTXORPEN);

		switch (type)
		{
		case IDC_R:
			Rectangle(hdc, x1, y1, x2, y2);
			break;
		case IDC_E:
			Ellipse(hdc, x1, y1, x2, y2);
			break;
		case IDC_L:
			MoveToEx(hdc, x1, y1, NULL);
			LineTo(hdc, x2, y2);
			break;
		}

		SetROP2(hdc, R2_COPYPEN);

		switch (type)
		{
		case IDC_R:
			Rectangle(hdc, x1, y1, x2, y2);
			break;
		case IDC_E:
			Ellipse(hdc, x1, y1, x2, y2);
			break;
		case IDC_L:
			MoveToEx(hdc, x1, y1, NULL);
			LineTo(hdc, x2, y2);
			finishedL = 1;
			break;
		case IDC_F:
		{
			// 
			finishedF = 1;
			// 
			break;
		}
		}

		ReleaseDC(hWnd, hdc);
		break;
	}

	case WM_DESTROY:
		DeleteObject(hPen);
		SendMessage(hFrame, WM_DESTROY, 0, 0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}


// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

INT_PTR CALLBACK Choice(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		SetWindowLong(hDlg, GWL_EXSTYLE, WS_EX_TOOLWINDOW);
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_R: case IDC_E: case IDC_L: case IDC_F:
			SetFocus(hDraw);
			SendMessage(hDraw, WM_COMMAND, wParam, 0);
			break;
		default:
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return (INT_PTR)FALSE;
}