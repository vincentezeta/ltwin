// Classifier.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"


HINSTANCE hInst = NULL;
HWND hFrame = NULL;
HHOOK hHookKey = NULL, hHookMouse = NULL;
bool ctrl = false, win = false;
char code = 0; int state = 0;
std::string binded = "abcdefghijklmnopqrstuvwxyz"; //= "";

std::vector<bool> allLine;
std::vector<POINT> allCenter;
std::vector<std::vector<POINT>> allPoint;
std::vector<std::vector<double>> allAngle;

double angle = 0, sumAngle = 0;
bool firstPoint = true, secondPoint = false;
int xCur = -1, yCur = -1, xCen = 0, yCen = 0;

extern "C" {

LRESULT CALLBACK LockProc(int nCode, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK MoveProc(int nCode, WPARAM wParam, LPARAM lParam);

EXPORT void TheHook(HWND hWnd)
{
	//MessageBox(NULL, L"SOS", L"TheHook", MB_OK);
	hFrame = hWnd;
	if (hHookKey == NULL)
		hHookKey = SetWindowsHookEx(WH_KEYBOARD_LL, LockProc, hInst, 0);
	if (hHookMouse == NULL)
		hHookMouse = SetWindowsHookEx(WH_MOUSE_LL , MoveProc, hInst, 0);
}

LRESULT CALLBACK LockProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0)
		return CallNextHookEx(hHookKey, nCode, wParam, lParam);
	
	KBDLLHOOKSTRUCT* info = (KBDLLHOOKSTRUCT*)lParam;
	DWORD key = info->vkCode;
	
	//MessageBox(NULL, L"SOS", L"LockProc", MB_OK);
	if (wParam == WM_KEYDOWN)
	{
		ctrl = ctrl || ((key == VK_LCONTROL || key == VK_RCONTROL) && !win);
		win  = win  || ((key == VK_LWIN     || key == VK_RWIN    ) && ctrl);
	}
	else if (wParam == WM_KEYUP)
	{
		ctrl = ctrl && !(key == VK_LCONTROL || key == VK_RCONTROL);
		win  = win  && !(key == VK_LWIN     || key == VK_RWIN    );
		if (!ctrl && !win && state != 0)
		{
			if (state == 2)
			{
				Classify();
				SendMessage(hFrame, MM_CHAR, code, 0);
			}
			state = 0;
			allLine.clear();
			allCenter.clear();
			allPoint.clear();
			allAngle.clear();
		}
	}
	
	return CallNextHookEx(hHookKey, nCode, wParam, lParam);
}

LRESULT CALLBACK MoveProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	if (nCode < 0 || !ctrl || !win)
		return CallNextHookEx(hHookKey, nCode, wParam, lParam);

	MOUSEHOOKSTRUCT* info = (MOUSEHOOKSTRUCT*)lParam;
	POINT p = info->pt;

	//MessageBox(NULL, L"SOS", L"MoveProc", MB_OK);
	if (wParam == WM_LBUTTONDOWN)
	{
		//MessageBox(NULL, L"SOS", L"d", MB_OK);

		allPoint.push_back(std::vector<POINT>());
		allAngle.push_back(std::vector<double>());
		allPoint.back().push_back(p);
		xCur = p.x; yCur = p.y;
		xCen += p.x; yCen += p.y;

		state = 1;
	}
	else if (wParam == WM_MOUSEMOVE && state == 1)
	{
		//MessageBox(NULL, L"SOS", L"m", MB_OK);
		
		double tmpa = atan2(1.0*yCur-p.y, 1.0*p.x-xCur);
		if (abs(tmpa-angle) > ATHRES*PI/180 || secondPoint)
		{
			angle = tmpa;
			if (!firstPoint)
			{
				sumAngle += angle/PI*180;
				if (angle < 0)
					sumAngle += 360;
				allPoint.back().push_back(p);
				allAngle.back().push_back(angle);
				xCur = p.x; yCur = p.y;
				xCen += p.x; yCen += p.y;
				secondPoint = false;
			}
			else
			{
				secondPoint = true;
				firstPoint = false;
			}
		}
	}
	else if (wParam == WM_LBUTTONUP && state == 1)
	{
		//MessageBox(NULL, L"SOS", L"u", MB_OK);
		
		if (p.x != xCur || p.y != yCur)
		{
			angle = atan2(1.0*yCur-p.y, 1.0*p.x-xCur);
			sumAngle += angle/PI*180;
			if (angle < 0)
				sumAngle += 360;
			allPoint.back().push_back(p);
			allAngle.back().push_back(angle);
			xCur = p.x; yCur = p.y;
			xCen += p.x; yCen += p.y;
		}

		float num = allPoint.back().size();
		POINT beg = allPoint.back()[0], cen;
		cen.x = xCen; cen.y = yCen;

		double tmpa = atan2(1.0*beg.y-yCur, 1.0*xCur-beg.x)/PI*180,
			meanAngle = sumAngle/(num-1);
		if (tmpa < 0)
			tmpa += 360;
		allLine.push_back(abs(tmpa-meanAngle) < ATHRES || abs(tmpa-360-meanAngle) < ATHRES || abs(tmpa+360-meanAngle) < ATHRES);
		allCenter.push_back(cen);
		
		std::ofstream ofs("out.txt", std::ios::app); //
		double first = allAngle.back()[0];
		ofs << (angle-first)/PI*180 << " | " << xCen/num << " ";
		ofs	<< yCen/num << " | " << num << " | " << tmpa << " ";
		ofs	<< meanAngle << " | " << allLine.back() << std::endl;
		ofs.close();
		
		angle = sumAngle = 0;
		xCen = yCen = 0;
		firstPoint = true;

		state = 2;
	}
	
	return CallNextHookEx(hHookKey, nCode, wParam, lParam);
}

EXPORT char GetCharID()
{
	if (state != 1)
		return code;
	return 0;
}

EXPORT void Bind(char b)
{
	int pos = binded.find(b);
	if (pos == std::string::npos)
		binded.push_back(b);
}

EXPORT void Unbind(char u)
{
	int pos = binded.find(u);
	if (pos != std::string::npos)
		binded.erase(pos, 1);
}

EXPORT void UnTheHook()
{
	UnhookWindowsHookEx(hHookKey);
	UnhookWindowsHookEx(hHookMouse);
}

EXPORT std::vector<std::vector<POINT>>* GetPoint()
{
	return &allPoint;
}

}