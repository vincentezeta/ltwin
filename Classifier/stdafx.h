// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <string>


#define EXPORT __declspec(dllexport)
#define MM_CHAR 30000 //
#define PI 3.14159
#define ATHRES 20
#define DTHRES 5.0
#define RTHRES 0.25
#define UNKNOWN '?' //

double dist(int x, int y);
bool distThres(POINT a, POINT b);
bool angleThres(double x, double base);
bool ratioThres(int x, int y, double base);
void maxMin(POINT& leftUp, POINT& rightDown, int type = -1);

bool checkBinded(std::string candid);

void Classify();
void ClassifyOne();
void ClassifyTwo();
void ClassifyThree();