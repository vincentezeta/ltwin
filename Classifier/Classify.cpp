#include "stdafx.h"


extern char code;
extern std::string binded;
extern std::vector<bool> allLine;
extern std::vector<POINT> allCenter;
extern std::vector<std::vector<POINT>> allPoint;
extern std::vector<std::vector<double>> allAngle;
POINT gLU, gRD;


bool checkBinded(std::string candid)
{
	int count = 0; char first = 0;
	for (unsigned int i = 0; i < candid.size(); ++i)
		if (binded.find(candid[i]) != std::string::npos)
		{
			++count;
			if (first == 0)
				first = candid[i];
			if (count > 1)
				return true;
		}

	if (count == 1)
		code = first;
	else
		code = UNKNOWN;
	
	return false;
}

double dist(int x, int y)
{
	return sqrt(1.0*x*x+1.0*y*y);
}

bool distThres(POINT a, POINT b)
{
	double coX = DTHRES/(gRD.x-gLU.x+1), coY = DTHRES/(gRD.y-gLU.y+1);
	return abs(a.x-b.x)*coX+abs(a.y-b.y)*coY < 1;
}

bool angleThres(double x, double base) // x: radian (-PI,+PI], base: degree [-180,180]
{
	double deg = x/PI*180;
	if (180-abs(base) < ATHRES/2)
		return abs(deg-base) < ATHRES || abs(deg+base) < ATHRES;
	return abs(deg-base) < ATHRES;
}

bool ratioThres(int x, int y, double base)
{
	double ratio = abs(x)/abs(y);
	return abs(ratio-base) < RTHRES;
}

void maxMin(POINT& leftUp, POINT& rightDown, int type)
{
	unsigned int begin = type;
	if (type < 0 || type >= allLine.size())
	{
		type = allLine.size()-1;
		begin = 0;
	}
	leftUp = allPoint[begin][0];
	rightDown = allPoint[begin][0];
	for (unsigned int i = begin; i <= type; ++i)
	for (unsigned int j = 0; j < allPoint[i].size(); ++j)
	{
		if (allPoint[i][j].x < leftUp.x)
			leftUp.x = allPoint[i][j].x;
		if (allPoint[i][j].y < leftUp.y)
			leftUp.y = allPoint[i][j].y;
		if (allPoint[i][j].x > rightDown.x)
			rightDown.x = allPoint[i][j].x;
		if (allPoint[i][j].y > rightDown.y)
			rightDown.y = allPoint[i][j].y;
	}
}


void Classify()
{
	//MessageBox(NULL, L"SOS", L"Classify", MB_OK);
	std::ofstream ofs("out.txt", std::ios::app); //
	ofs << std::endl;
	for (unsigned int i = 0; i < allLine.size(); ++i)
	{
		for (unsigned int j = 0; j < allPoint[i].size(); ++j)
			ofs << allPoint[i][j].x << " " << allPoint[i][j].y << " ";
		ofs << std::endl;
		for (unsigned int j = 0; j < allAngle[i].size(); ++j)
			ofs << allAngle[i][j]/PI*180 << " ";
		ofs << std::endl;
		ofs << allLine[i] << " " << allCenter[i].x << " " << allCenter[i].y << std::endl << std::endl;
	}
	//ofs.close();


	if (!checkBinded("abcdefghijklmnopqrstuvwxyz"))
		return;

	maxMin(gLU, gRD);
	ofs << gLU.x << " " << gLU.y << " " << gRD.x << " " << gRD.y << std::endl << std::endl; //
	ofs.close();

	switch (allPoint.size())
	{
	case 1:
		ClassifyOne();
		break;
	case 2:
		ClassifyTwo();
		break;
	case 3:
		ClassifyThree();
		break;
	default:
		//MessageBox(NULL, L"SOS", L"Null", MB_OK);
		code = UNKNOWN;
	}
}

void ClassifyTwo()
{
	//MessageBox(NULL, L"SOS", L"Two", MB_OK);

	if (!checkBinded("dfijkpqtxy"))
		return;
	
	POINT oneF = allPoint[0][0], oneL = allPoint[0].back();
	double tmpa = atan2(1.0*oneF.y-oneL.y,1.0*oneL.x-oneF.x),
		ath = ATHRES*PI/180;
	if (allLine[0] && (angleThres(tmpa, 180) || angleThres(tmpa, 0)))
	{
		checkBinded("t");
		return;
	}

	POINT lu, rd;
	int small = 15; //
	maxMin(lu, rd, 0);
	if (rd.y-lu.y <= small && rd.x-lu.x <= small)
	{
		if (!checkBinded("ij"))
			return;

		double tmpi = allAngle[1].back();
		if (tmpi < -(PI+ath)/2 || tmpi > (PI+ath)/2)
			checkBinded("j");
		else
			checkBinded("i");
		return;
	}

	if (!checkBinded("dfkpqtxy"))
		return;

	POINT twoF = allPoint[1][0], twoL = allPoint[1].back();
	tmpa = atan2(1.0*twoF.y-twoL.y,1.0*twoL.x-twoF.x);
	if (allLine[1] && (angleThres(tmpa, 180) || angleThres(tmpa, 0)))
	{
		if (!checkBinded("ft"))
			return;

		/*POINT oneC = allCenter[0], twoC = allCenter[1];
		oneC.x /= allPoint[0].size(); oneC.y /= allPoint[0].size();
		twoC.x /= allPoint[1].size(); twoC.y /= allPoint[1].size();*/

		double tmpf = allAngle[0][0];
		if (tmpf > (PI+ath)/2)
			checkBinded("f");
		else if (tmpf < (PI-ath)/2 && tmpf > -(PI+ath)/2)
			checkBinded("t");
		else
			code = UNKNOWN;
		return;
	}

	if (!checkBinded("dkpqxy"))
		return;

	POINT oneM = allPoint[0].back(), twoM = allPoint[1].back();
	oneM.x = (oneM.x+allPoint[0][0].x)/2;
	oneM.y = (oneM.y+allPoint[0][0].y)/2;
	twoM.x = (twoM.x+allPoint[1][0].x)/2;
	twoM.y = (twoM.y+allPoint[1][0].y)/2;
	if (allLine[1])
	{
		if (!checkBinded("dqxy"))
			return;
		
		if (allLine[0])
		{
			if (!checkBinded("xy"))
				return;

			if (distThres(oneM, twoM))
				checkBinded("x");
			else if (distThres(allPoint[0].back(), twoM))
				checkBinded("y");
			else
				code = UNKNOWN;
			return;
		}

		if (!checkBinded("dq"))
			return;

		POINT oneC = allCenter[0];
		oneC.x /= allPoint[0].size();
		oneC.y /= allPoint[0].size();
		if (oneC.y > twoM.y && distThres(allPoint[0].back(), allPoint[1].back()))
			checkBinded("d");
		else if (oneC.y < twoM.y && distThres(allPoint[0][0], allPoint[1][0]))
			checkBinded("q");
		else
			code = UNKNOWN;
		return;
	}

	if (!checkBinded("kpx"))
		return;

	double tmpp = allAngle[1].back(), tmpk = allAngle[1][0];
	if (allLine[0])
	{
		if (!checkBinded("kp"))
			return;

		if (tmpk > -(PI-ath)/2 && tmpk < (PI-ath)/2 && (tmpp < -(PI+ath)/2 || tmpp > (PI+ath)/2))
			checkBinded("p");
		else if (tmpk < -(PI+ath)/2 && tmpp < 0 && tmpp > -PI/2)
			checkBinded("k");
		else
			code = UNKNOWN;
		return;
	}

	checkBinded("x");
	return;
}

void ClassifyThree()
{
	//MessageBox(NULL, L"SOS", L"Three", MB_OK);
	
	if (!checkBinded("km"))
		return;

	if (allLine[0])
	{
		if (allLine[1] && allLine[2])
			checkBinded("k");
		else if (!allLine[1] && !allLine[2])
			checkBinded("m");
		else
			code = UNKNOWN;
	}
	else
		code = UNKNOWN;
}